var app2 = angular.module('app.controllers', []);

app2.controller('timeLineCtrl', function($rootScope, $document, $timeout, $scope, $ionicScrollDelegate) {

	//})

	//var ExampleCtrl = function($rootScope, $document, $timeout, $scope) {

	$scope.side = '';

    $scope.event =
        {
            badgeClass : 'bg-royal',
			badgeIconClass : 'ion-checkmark',
            title: '',
			when : '3 hours ago via Twitter',
			content : 'Some awesome content.',
        };

	/*$scope.events = [{
		badgeClass : '',
		badgeIconClass : 'ion-ionic',
		title : 'First heading',
		when : '11 hours ago via Twitter',
		content : 'Some awesome content.'
	}, {
		badgeClass : 'bg-positive',
		badgeIconClass : 'ion-gear-b',
		title : 'Second heading',
		when : '12 hours ago via Twitter',
		content : 'More awesome content.'
	}, {
		badgeClass : 'bg-balanced',
		badgeIconClass : 'ion-person',
		title : 'Third heading',
		titleContentHtml : '<img class="img-responsive" src="http://www.freeimages.com/assets/183333/1833326510/wood-weel-1444183-m.jpg">',
		contentHtml : lorem,
		footerContentHtml : '<a href="">Continue Reading</a>'
	}]; */

	$scope.addEvent = function() {

        $scope.event.location =
            {
                 googlePlaceId: $rootScope.lastGMapLookupLocationResult.place_id,
                 address: $rootScope.lastGMapLookupLocationResult.formatted_address,
                 lat: $rootScope.lastGMapLookupLocationResult.geometry.location.lat(),
                 lng: $rootScope.lastGMapLookupLocationResult.geometry.location.lng()
		    };

		$rootScope.cogo.trip.destinations[0].events.push($scope.event

        /*    {
			badgeClass : 'bg-royal',
			badgeIconClass : 'ion-checkmark',
			title : 'Fourth event',
			when : '3 hours ago via Twitter',
			content : 'Some awesome content.',
            location: {
                 googlePlaceId: $rootScope.cogo.trip.lastGMapLookupLocationResult.place_id,
                 address: $rootScope.cogo.trip.lastGMapLookupLocationResult.formatted_address,
                 lat: $rootScope.cogo.trip.lastGMapLookupLocationResult.geometry.location.lat(),
                 lng: $rootScope.cogo.trip.lastGMapLookupLocationResult.geometry.location.lng()
             }
		} */
                                                        );
		$ionicScrollDelegate.resize();
        $scope.event = {};
	};

	// optional: not mandatory (uses angular-scroll-animate)
	$scope.animateElementIn = function($el) {
		$el.removeClass('timeline-hidden');
		$el.addClass('bounce-in');
	};

	// optional: not mandatory (uses angular-scroll-animate)
	$scope.animateElementOut = function($el) {
		$el.addClass('timeline-hidden');
		$el.removeClass('bounce-in');
	};

	$scope.reExecuteAnimation = function(){
		TM = document.getElementsByClassName('tm');
		for (var i = 0; i < TM.length; i++) {
	        removeAddClass(TM[i], 'bounce-in');
	    }
	}

/*	$scope.rePerformAnimation = function(){
	   $scope.reExecuteAnimation();
	}

	$scope.leftAlign = function() {
		$scope.side = 'left';
		$ionicScrollDelegate.resize();

		$scope.reExecuteAnimation();
	}

	$scope.rightAlign = function() {
		$scope.side = 'right';
		$ionicScrollDelegate.resize();

		$scope.reExecuteAnimation();
	}

	$scope.defaultAlign = function() {
		$scope.side = '';
		$ionicScrollDelegate.resize();

		$scope.reExecuteAnimation();
		// or 'alternate'
	} */
});

app2.controller('TimelineTableController', function($scope, $rootScope) {

});

app2.controller('addEventCtrl', function($scope, $ionicModal) {
  $ionicModal.fromTemplateUrl('templates/addevent.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
});

function hasClass(el, className) {
  if (el.classList)
    return el.classList.contains(className)
  else
    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
}

function addClass(el, className) {
  if (el.classList)
    el.classList.add(className)
  else if (!hasClass(el, className)) el.className += " " + className
}

function removeClass(el, className) {
  if (el.classList)
    el.classList.remove(className)
  else if (hasClass(el, className)) {
    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
    el.className=el.className.replace(reg, ' ')
  }
}

function removeAddClass(el, className){
	removeClass(el, className)
	  setTimeout(function(){
	  	addClass(el, className);
	  }, 1)

}

//angular.module('example').controller('ExampleCtrl', ExampleCtrl);
