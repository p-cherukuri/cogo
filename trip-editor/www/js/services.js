var app3 = angular.module('app.services', []);
app3.factory('Auth', function($firebaseAuth, FirebaseUrl) {
   var ref = new Firebase(FirebaseUrl);
   var auth = $firebaseAuth(ref);
    
    return auth;
});

/* app3.factory('TripStore', function($firebaseArray) {
    var ref = firebase.database().ref().child("trips"); //downloading data into local object
    
    $rootScope.trips = $firebaseArray(ref);
    
    $rootScope.lastSelectedStartDate = new Date();
    $rootScope.lastSelectedEndDate = new Date();
    
    $scope.createTrip = function() {
             return {
                title: '',
                destinations: [
                    {
                        title: '',
                        location: {
                            googlePlaceId: '',
                            address: '',
                            lat: 0,
                            lng: 0
                        },
                        events: [
                            {
                                badgeClass: '',
                                badgeIconClass: 'ion-ionic',
                                title: '',
                                when: '',
                                content: '',
                                location: {
                                    googlePlaceId: '',
                                    address: '',
                                    lat: 0,
                                    lng: 0
                                }
                            }
                        ]
                    }
                ]
            };
    };
    
    $scope.$on('modal.shown', function() {

        $rootScope.newTrip = $scope.createTrip();
        console.log("New trip initialized");
    });
    
    $scope.addDestLocation = function() {
        $rootScope.newTrip.destinations[0].location.googlePlaceId = $rootScope.lastGMapLookupLocationResult.place_id;
        $rootScope.newTrip.destinations[0].location.address = $rootScope.lastGMapLookupLocationResult.formatted_address;
        $rootScope.newTrip.destinations[0].location.lat = $rootScope.lastGMapLookupLocationResult.geometry.location.lat();
        $rootScope.newTrip.destinations[0].location.lng = $rootScope.lastGMapLookupLocationResult.geometry.location.lng();
        console.log("First Destination location added");
    };
    
    $scope.addDates = function() {
        $rootScope.newTrip.destinations[0].startDate = $rootScope.lastSelectedStartDate.getTime();
        $rootScope.newTrip.destinations[0].endDate = $rootScope.lastSelectedEndDate.getTime();
    };
    
    $scope.addTrip = function() {
        $rootScope.trips.$add($rootScope.newTrip).then(function(ref) {
            console.log("New trip created");
            var id = ref.key;
            console.log("added record with id " + id);
            $rootScope.currentTrip = $rootScope.trips.$getRecord(id);
            console.log("Current trip index is " + $rootScope.trips.$indexFor(id));
        })
        .catch(function(err) {
            throw err;
        });
    };

}); */
/* var notes = angular.fromJson(window.localStorage['events'] || '[]');

	  function persist() {
		window.localStorage['events'] = angular.toJson(events);
	  }
	  
	  return {

	    list: function() {
	      return events;
	    },

	    get: function(noteId) {
	      for (var i = 0; i < notes.length; i++) {
	        if (notes[i].id === noteId) {
	          return notes[i];
	        }
	      }
	      return undefined;
	    },

	    create: function(note) {
	      notes.push(note);
		  persist();
	    },

	    update: function(note) {
	      for (var i = 0; i < notes.length; i++) {
	        if (notes[i].id === note.id) {
	          notes[i] = note;
			  persist();
	          return;
	        }
	      }
	    },
		
		move: function(note, fromIndex, toIndex) {
			notes.splice(fromIndex, 1);
			notes.splice(toIndex, 0, note);
			persist();
		},
		
		remove: function(noteId) {
			for (var i = 0; i < notes.length; i++) {
				if (notes[i].id === noteId) {
				  notes.splice(i, 1);
				  persist();
				  return;
				}
			}	
		}

	  }; */