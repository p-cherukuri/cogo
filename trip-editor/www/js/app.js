// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
(function() {

<<<<<<< HEAD
var app = angular.module('app',
		  [
			'ionic',
			'ngCordova',
			'app.controllers',
			'app.services',
			'ngSanitize',
			'ui.router',
			'angular-timeline',
			'angular-scroll-animate',
			'ion-google-autocomplete',
			'ion-datetime-picker',
			'firebase'
		  ]);
     app.constant('FirebaseUrl', 'https://cogo-143622.firebaseio.com');

app.config(function($stateProvider, $urlRouterProvider) {
    
    // auth state
 /* $stateProvider.state('sign-in', {
    url: '/sign-in',
    cache: false,
    templateUrl: 'templates/auth/sign-in.html',
    //controller: 'AuthCtrl',
    resolve: {
      // controller will not be loaded until $waitForSignIn resolves
      // Auth refers to our $firebaseAuth wrapper in the example above
      "auth": ["Auth", function(Auth) {
        // $waitForSignIn returns a promise so the resolve waits for it to complete
        return Auth.$waitForSignIn();
      }]
    }
  });

  $stateProvider.state('sign-in-with-email', {
    url: '/sign-in-with-email',
    cache: false,
    templateUrl: 'templates/auth/sign-in-with-email.html',
    //controller: 'AuthCtrl',
    resolve: {
      // controller will not be loaded until $waitForSignIn resolves
      // Auth refers to our $firebaseAuth wrapper in the example above
      "auth": ["Auth", function(Auth) {
        // $waitForSignIn returns a promise so the resolve waits for it to complete
        return Auth.$waitForSignIn();
      }]
    }
  }); */
    
    $stateProvider.state('login', {
        url: '/login',
        controller: 'AuthCtrl as authCtrl',
        templateUrl: 'templates/auth/login.html',
        resolve: {
          requireNoAuth: function($state, Auth){
            return Auth.$requireAuth().then(function(auth){
              $state.go('/home');
            }, function(error){
              throw error;
            });
          }
        }
    });
    
    $stateProvider.state('register', {
        url: '/register',
        controller: 'AuthCtrl as authCtrl',
        templateUrl: 'templates/auth/register.html',
        resolve: {
          requireNoAuth: function($state, Auth){
            return Auth.$requireAuth().then(function(auth){
              $state.go('/home');
            }, function(error){
              throw error;
            });
          }
        }
    });
    
    // Home tabs
    $stateProvider.state('home', {
        url: '/home',
        templateUrl: "templates/home.html",
        redirectTo: 'home.my-trips'
    });
    
    $stateProvider.state('home.inbox', {
        url: '/inbox',
        views: {
            'inbox': {
                templateUrl: 'templates/inbox.html'
            }
        }                
    });
    
    $stateProvider.state('home.explore', {
        url: '/explore',
        views: {
            'explore': {
                templateUrl: 'templates/explore.html'
            }
        }                
    });
    
    $stateProvider.state('home.trip-feed', {
        url: '/trip-feed',
        views: {
            'trip-feed': {
                templateUrl: 'templates/trip-feed.html'
            }
        }                
    });
    
    $stateProvider.state('home.my-trips', {
        url: '/my-trips',
        views: {
            'my-trips': {
                templateUrl: 'templates/my-trips.html'
            }
        }                
    });
    
    $stateProvider.state('home.trip', {
        url: '/my-trips/:tripId',
        views: {
            'my-trips': {
                templateUrl: 'templates/trip.html'
            }
        }
    });
    
    //Trip editor states
	$stateProvider.state('planner', {
		url: '/planner',
		templateUrl: 'templates/planner.html'
	});
    
    $stateProvider.state('timelineview',  {
            url: '/timelineview',
                    templateUrl: "templates/timelineview.html"
                    //controller: "timelineViewCtrl"
        }
    );
	
	$stateProvider.state('timelineTableView',  {
=======
  var app = angular.module('app',
            [
              'ionic',
              'ngCordova',
              'timeLine.controllers',
              'timeLine.services',
              'ngSanitize',
              'ui.router',
              'angular-timeline',
              'angular-scroll-animate',
              'ion-google-autocomplete',
              'ion-datetime-picker'
            ]);

  app.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider.state('planner', {
      url: '/planner',
      templateUrl: 'templates/planner.html'
    });

    $stateProvider.state('timelineview',  {
      url: '/timelineview',
      templateUrl: "templates/timelineview.html"
      //controller: "timelineViewCtrl"
    });

    $stateProvider.state('timelineTableView',  {
>>>>>>> 8b5282d98a638891c5dafba3dbbeaeac75e9562b
      url: '/timelinetableview',
      templateUrl: "templates/timelinetableview.html"
      //controller: "timelineViewCtrl"
    });
<<<<<<< HEAD
    
=======

>>>>>>> 8b5282d98a638891c5dafba3dbbeaeac75e9562b
    $stateProvider.state('mapview', {
      url: '/mapview',
      templateUrl: "templates/mapview.html"
      //controller: "mapCtrl"
    });

    $stateProvider.state('details', {
      url: '/details',
      templateUrl: 'templates/details.html'
    });

    $stateProvider.state('ideas', {
      url: '/ideas',
      templateUrl: 'templates/ideas.html'
    });

    $stateProvider.state('search', {
      url: '/search',
      templateUrl: 'templates/search.html'
    });

    $stateProvider.state('settings', {
      url: '/settings',
      templateUrl: 'templates/settings.html'
    });

    $stateProvider.state('timeLine', {
      url: '/timeline',
      templateUrl: 'templates/timeline.html',
      controller: 'timeLineCtrl'
    });
<<<<<<< HEAD
	
	$urlRouterProvider.otherwise('/planner');
});   
    
app.controller("TripStoreCtrl", function($scope, $rootScope, $firebaseArray) {
    var ref = firebase.database().ref().child("trips"); //downloading data into local object
    
    $rootScope.trips = $firebaseArray(ref);
    
    $rootScope.lastSelectedStartDate = new Date();
    $rootScope.lastSelectedEndDate = new Date();
   /* $scope.createTrip = function() {
             $scope.trips.$add({
                title: $scope.tripTitle,
                destinations: [
                    {
                        title: '',
                        location: $rootScope.cogo.trip.lastGMapLookupLocationResult,
                        events: [
                            {
                                badgeClass: '',
                                badgeIconClass: 'ion-ionic',
                                title: '',
                                when: '',
                                content: '',
                                location: {
                                    googlePlaceId: '',
                                    address: '',
                                    lat: 0,
                                    lng: 0
                                }
                            }
                        ]
                    }
                ]
            }).then(function(ref) {
            console.log("New trip created");
        })
        .catch(function(err) {
            throw err;
        });
    }; */
    
    $scope.createTrip = function() {
             return {
                title: '',
                destinations: [
                    {
                        title: '',
                        location: {
                            googlePlaceId: '',
                            address: '',
                            lat: 0,
                            lng: 0
                        },
                        events: [
                            {
                                badgeClass: '',
                                badgeIconClass: 'ion-ionic',
                                title: '',
                                when: '',
                                content: '',
                                location: {
                                    googlePlaceId: '',
                                    address: '',
                                    lat: 0,
                                    lng: 0
                                }
                            }
                        ]
                    }
                ]
            };
    };
    
    $scope.$on('modal.shown', function() {

        $rootScope.newTrip = $scope.createTrip();
        console.log("New trip initialized");
    });
    
    $scope.addDestLocation = function() {
        $rootScope.newTrip.destinations[0].location.googlePlaceId = $rootScope.lastGMapLookupLocationResult.place_id;
        $rootScope.newTrip.destinations[0].location.address = $rootScope.lastGMapLookupLocationResult.formatted_address;
        $rootScope.newTrip.destinations[0].location.lat = $rootScope.lastGMapLookupLocationResult.geometry.location.lat();
        $rootScope.newTrip.destinations[0].location.lng = $rootScope.lastGMapLookupLocationResult.geometry.location.lng();
        console.log("First Destination location added");
    };
    
    $scope.addDates = function() {
        $rootScope.newTrip.destinations[0].startDate = $rootScope.lastSelectedStartDate.getTime();
        $rootScope.newTrip.destinations[0].endDate = $rootScope.lastSelectedEndDate.getTime();
    };
    
     /*$scope.addDate = function() {
        $rootScope.newTrip.destinations[0].location.googlePlaceId = $rootScope.lastGMapLookupLocationResult.place_id;
        $rootScope.newTrip.destinations[0].location.address = $rootScope.lastGMapLookupLocationResult.formatted_address;
        $rootScope.newTrip.destinations[0].location.lat = $rootScope.lastGMapLookupLocationResult.geometry.location.lat();
        $rootScope.newTrip.destinations[0].location.lng = $rootScope.lastGMapLookupLocationResult.geometry.location.lng();
        console.log("First Destination location added");
    };*/
    
    $scope.addTrip = function() {
        $rootScope.trips.$add($rootScope.newTrip).then(function(ref) {
            console.log("New trip created");
            var id = ref.key;
            console.log("added record with id " + id);
            $rootScope.currentTrip = $rootScope.trips.$getRecord(id);
            console.log("Current trip index is " + $rootScope.trips.$indexFor(id));
        })
        .catch(function(err) {
            throw err;
        });
    };
    
});
    
  /*  $scope.$on('modal.shown', function() {

        $rootScope.newTrip = $scope.createTrip();
        console.log("New trip initialized");
    });
    
    $scope.addTrip = function() {
        $rootScope.trips.$add($rootScope.newTrip).then(function(ref) {
            console.log("New trip created");
        })
        .catch(function(err) {
            throw err;
        });
    }; */
    
    
  /*  $scope.createTrip = function() {
         return {
            title: '',
            destinations: [
                {
                    title: '',
                    events: [
                        {
                            badgeClass: '',
                            badgeIconClass: 'ion-ionic',
                            title: '',
                            when: '',
                            content: '',
                            location: {
                                googlePlaceId: '',
                                address: '',
                                lat: 0,
                                lng: 0
                            }
                        }
                    ]
                }
            ]
        };
    };
    
    $scope.createDest = function() {
        return {
            location: {
                        
            },
            title: '',
            events: [
                {
                    badgeClass: '',
                    badgeIconClass: 'ion-ionic',
                    title: '',
                    when: '',
                    content: '',
                    location: {
                        googlePlaceId: '',
                        address: '',
                        lat: 0,
                        lng: 0
                    }
                }
            ]
        };
    };
    
    $scope.createEvent = function() {
        return {
            badgeClass: '',
            badgeIconClass: 'ion-ionic',
            title: '',
            when: '',
            content: '',
            location: {
                googlePlaceId: '',
                address: '',
                lat: 0,
                lng: 0
            }
        };
    };
    
    $scope.$on('modal.shown', function() {
        
    
        var newTrip = $scope.createTrip();
   // $rootScope.dest = $scope.createDest();
   // $rootScope.event = $scope.createEvent();
    
        $rootScope.trips.$add(newTrip).then(function(ref) {
            var id = ref.key;
            console.log("added record with id " + id);
            $rootScope.currentTrip = $rootScope.trips.$getRecord(id);
            console.log("Current trip index is " + $rootScope.trips.$indexFor(id));
        }).catch(function(err) {
            throw err;  
        });
    });
    
   $scope.saveTrip = function() {
        
        $rootScope.currentTrip.destinations[0].location = 
            {
                 googlePlaceId: $rootScope.cogo.trip.lastGMapLookupLocationResult.place_id,
                 address: $rootScope.cogo.trip.lastGMapLookupLocationResult.formatted_address,
                 lat: $rootScope.cogo.trip.lastGMapLookupLocationResult.geometry.location.lat(),
                 lng: $rootScope.cogo.trip.lastGMapLookupLocationResult.geometry.location.lng()
		    };
        
        $rootScope.trips.$save($rootScope.currentTrip).then(function(ref) {
            
            console.log("Trip saved");
        })
        .catch(function(err) {
            throw err;
        });
    };
    
    $scope.deleteTrip = function() {
        $rootScope.trips.$remove($rootScope.currentTrip).then(function(ref) {
            console.log("New trip canceled");
        })
        .catch(function(err) {
            throw err;
        });
    }; */
    
    
    //....trip.destinations.$add($rootScope.dest)
    //....trip.dest.events.$add($rootScope.event)
    
    //<input type='text' ng-model='trip.title' name="Title"></input>
    //<input type='text' ng-model='trip.destinations[trip.cdi].location.lat' name="lat"></input>
    //....firebase....$save($rootScope.trip);
    
    // Creating sync-ed array
    /* $rootScope.trips = $firebaseArray(ref);
    // Adding new items to array
    $scope.addTrip = function() {
        $scope.trips.$add({
            title: '',
            destinations: [
                {
                    location: {
                         
                    },
                    title: '',
                    events: [
                        {
                            badgeClass : '',
                            badgeIconClass : 'ion-ionic',
                            title : '',
                            when : '',
                            content : '',
                             location: {
                                 GooglePlaceId: '',
                                 Address: '',
                                 Lat: 0,
                                 Long: 0
                             }                        
                        }
                    ]
                }
                
            ]
            
        })
        
    }; 
    
}); */
    
app.controller('AuthCtrl', function(Auth, $state) {
   var authCtrl = this;
    
    authCtrl.user = {
        email: '',
        password: ''
    };
    
    authCtrl.login = function (){
        Auth.$authWithPassword(authCtrl.user).then(function(auth){
            $state.go('/home');
        }, function (error){
            authCtrl.error = error;
            throw error;
        });
    };
    
    authCtrl.register = function (){
        Auth.$createUser(authCtrl.user).then(function(user){
            authCtrl.login();
        }, function (error){
            authCtrl.error = error;
            throw error;
        });
    };
});
    
app.controller('addTripCtrl', ['$scope', function($scope) {
    
    $scope.templates =
        [{ name: 'newtripdest', url: 'templates/newtripdest.html'},
         { name: 'newtripinvite', url: 'templates/newtripinvite.html'}];
    $scope.pos = 0;
    $scope.template = $scope.templates[$scope.pos];
    $scope.nextTripStep = function() {
        $scope.pos = $scope.pos + 1;
        $scope.template = $scope.templates[$scope.pos];
    };
    $scope.prevTripStep = function() {
        $scope.pos = $scope.pos - 1;
        $scope.template = $scope.templates[$scope.pos];
    };
    
}]);
=======
>>>>>>> 8b5282d98a638891c5dafba3dbbeaeac75e9562b

    $urlRouterProvider.otherwise('/planner');
  });

  // Toggle Timeline and Map View
  app.controller('PlannerCtrl', ['$scope', function($scope) {

    $scope.templates =
<<<<<<< HEAD
        [{ name: 'timelineview', url: 'templates/timelinetableview.html'},
         { name: 'map', url: 'templates/mapview.html'}];
    $scope.buttons = 
        [{name: 'map', icon: 'ion-map'},
         {name: 'timeline', icon: 'ion-calendar'}];
    $scope.pos = 0;
=======
    [{ name: 'timelineview', url: 'templates/timelineview.html'},
    { name: 'map', url: 'templates/mapview.html'}];
  $scope.buttons =
    [{name: 'map', icon: 'ion-map'},
    {name: 'timeline', icon: 'ion-calendar'}];
  $scope.pos = 0;
  $scope.template = $scope.templates[$scope.pos];
  $scope.button = $scope.buttons[$scope.pos];
  $scope.toggleView = function() {
    /*if($scope.template.url = 'templates/timelineview.html')
      { $scope.template = $scope.templates[1];
      }
      else if($scope.template.url = 'templates/mapview.html') {
      $scope.template = $scope.templates[0];
      } */
    $scope.pos = ($scope.pos + 1)% 2;
>>>>>>> 8b5282d98a638891c5dafba3dbbeaeac75e9562b
    $scope.template = $scope.templates[$scope.pos];
    $scope.button = $scope.buttons[$scope.pos];
  };
  }]);

  // Map View Controller

  app.controller('MapCtrl', function($scope, $rootScope, $state, $cordovaGeolocation) {
    var options = {timeout: 10000, enableHighAccuracy: true};

    /* $cordovaGeolocation.getCurrentPosition(options).then(function(position){

       var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

       var mapOptions = {
       center: latLng,
       zoom: 15,
       mapTypeId: google.maps.MapTypeId.ROADMAP
       };


       $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

       google.maps.event.trigger($scope.map, "resize");
    //Wait until the map is loaded
    google.maps.event.addListenerOnce($scope.map, 'idle', function() {

    var events = $rootScope.cogo.trip.destinations[0].events;

    for (var i=0;i<events.length;i++) {
    var event = events[i];
    var latlong = new google.maps.LatLng(event.location.lat, event.location.lng);
    var marker = new google.maps.Marker({
    map: $scope.map,
    animation: google.maps.Animation.DROP,
    position: latlong
    });

    var infoWindow = new google.maps.InfoWindow({
    content: "This is your location"
    });

    google.maps.event.addListener(marker, 'click', function () {
    infoWindow.open($scope.map, marker);
    });

    }

    });


    }, function(error){
    console.log("Could not get location");
    }); */

    $cordovaGeolocation.getCurrentPosition(options).then(function(position){

      var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      var mapOptions = {
        center: latLng,
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP
      };


      $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

      google.maps.event.trigger($scope.map, "resize");
      //Wait until the map is loaded
      google.maps.event.addListenerOnce($scope.map, 'idle', function() {

        var events = $rootScope.cogo.trip.destinations[0].events;
        var bounds = new google.maps.LatLngBounds();

        for ( var i=0; i<events.length; i++ ) {
          var event = events[i];
          if (event.mock) {
            continue;
          }
          var latlong = new google.maps.LatLng(event.location.lat, event.location.lng);
          bounds.extend(latlong);
          var marker = new google.maps.Marker({
            map: $scope.map,
              animation: google.maps.Animation.DROP,
              position: latlong,
              content: event.title
          });

          var infoWindow = new google.maps.InfoWindow({
          });

          google.maps.event.addListener(marker, 'click', function (e) {
            infoWindow.setContent(this.position);
            infoWindow.setContent(this.content);
            infoWindow.open(this.get('map'), this);
          });

          /* marker.addListener('click', function() {
             infowindow.open(marker.get('map'), marker);
             }); */
        }

<<<<<<< HEAD
          $scope.map.fitBounds(bounds);
      });   
      
      }, function(error){
        console.log("Could not get location");
  }); 
});
    
app.controller('NewTripCtrl', function($scope, $ionicModal) {
  $ionicModal.fromTemplateUrl('templates/newtrip.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
});
    
app.controller('addEventCtrl', function($scope, $ionicModal) {
  $ionicModal.fromTemplateUrl('templates/addevent.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
=======
        $scope.map.fitBounds(bounds);
      });

    }, function(error){
      console.log("Could not get location");
    });
>>>>>>> 8b5282d98a638891c5dafba3dbbeaeac75e9562b
  });

  app.controller('addEventCtrl', function($scope, $ionicModal) {
    $ionicModal.fromTemplateUrl('templates/addevent.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });
    $scope.openModal = function() {
      $scope.modal.show();
    };
    $scope.closeModal = function() {
      $scope.modal.hide();
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function() {
      // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
      // Execute action
    });
  });

  app.controller('gMapsAutoFillCtrl', function ($scope, $rootScope) {

    $scope.data = {};

    //Optional
    $scope.countryCode = 'US';

    //Optional
    $scope.onAddressSelection = function (location) {
<<<<<<< HEAD
        $rootScope.lastGMapLookupLocationResult = location;
        console.log($rootScope.lastGMapLookupLocationResult);
        //Do something
        alert(location.formatted_address);
    };
});
    
app.controller('AppCtrl', function($scope) {

});

/* app.controller('PicturesCtrl', function($scope) {
  $scope.timeline = [{
    date: new Date(),
    title: "I am here",
    author: "Ludo Anderson",
    profilePicture: "https://upload.wikimedia.org/wikipedia/en/7/70/Shawn_Tok_Profile.jpg",
    text: "Lorem ipsum dolor sit amet",
    type: "location"

  }, {
    date: new Date(),
    title: "For my friends",
    author: "Sara Orwell",
    profilePicture: "https://lh5.googleusercontent.com/-ZadaXoUTBfs/AAAAAAAAAAI/AAAAAAAAAGA/19US52OmBqc/photo.jpg",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
    type: "text"

  }, {
    date: new Date(),
    title: "Look at my video!",
    author: "Miranda Smith",
    profilePicture: "https://static.licdn.com/scds/common/u/images/apps/plato/home/photo_profile_headshot_200x200_v2.jpg",
    text: "Lorem ipsum dolor sit amet",
    type: "video"

  }, {
    date: new Date(),
    title: "Awesome picture",
    author: "John Mybeweeg",
    profilePicture: "http://www.lawyersweekly.com.au/images/LW_Media_Library/LW-602-p24-partner-profile.jpg",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
    type: "picture"
  }]
});

app.controller('IconsCtrl', function($scope) {
  $scope.timeline = [{
    date: new Date(),
    title: "Awesome picture",
    author: "John Mybeweeg",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
    type: "picture"
  }, {
    date: new Date(),
    title: "Look at my video!",
    author: "Miranda Smith",
    text: "Lorem ipsum dolor sit amet",
    type: "video"

  }, {
    date: new Date(),
    title: "I am here",
    author: "Ludo Anderson",
    text: "Lorem ipsum dolor sit amet",
    type: "location"

  }, {
    date: new Date(),
    title: "For my friends",
    author: "Sara Orwell",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
    type: "text"

  }]
}); */

app.run(['$rootScope', '$ionicPlatform', '$state', function($rootScope, $ionicPlatform, $state) {
    $rootScope.$on('$stateChangeSuccess', function(evt, to, params) {
      if (to.redirectTo) {
        evt.preventDefault();
        $state.go(to.redirectTo, params, {location: 'replace'})
      }
    });
    
   /* $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
    // We can catch the error thrown when the $requireSignIn promise is rejected
    // and redirect the user back to the home page
    if (error === "AUTH_REQUIRED") {
      $state.go("sign-in");
    }
  });

  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
    if((toState.name === "sign-in" || toState.name === "sign-in-with-email" ) && $rootScope.firebaseUser){
      event.preventDefault();
    }
  });


  Auth.$onAuthStateChanged(function(firebaseUser) {

    // check user auth state
    if(firebaseUser){
      $rootScope.firebaseUser = firebaseUser;
      console.log("Log In");

    }
    else {
      // logout
      if(typeof facebookConnectPlugin === 'object'){
        facebookConnectPlugin.getLoginStatus(
          function(success){
            if(success.status == "connected"){
              facebookConnectPlugin.logout(
                function(status){
                  console.log("logOut success: " + status);
                },
                function(error){
                  console.log("logOut error: " + error);
                });
            }

          }, function(failure){

          });


      }

      $rootScope.firebaseUser = 0;
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
      $state.go("sign-in");
      console.log("Log Out");
    }
  }); */
    
=======
      $rootScope.cogo.trip.lastGMapLookupLocationResult = location;
      //Do something
      alert(location.formatted_address);
    };
  });

  app.controller('AppCtrl', function($scope) {

  });

  /* app.controller('PicturesCtrl', function($scope) {
     $scope.timeline = [{
     date: new Date(),
     title: "I am here",
     author: "Ludo Anderson",
     profilePicture: "https://upload.wikimedia.org/wikipedia/en/7/70/Shawn_Tok_Profile.jpg",
     text: "Lorem ipsum dolor sit amet",
     type: "location"

     }, {
     date: new Date(),
     title: "For my friends",
     author: "Sara Orwell",
     profilePicture: "https://lh5.googleusercontent.com/-ZadaXoUTBfs/AAAAAAAAAAI/AAAAAAAAAGA/19US52OmBqc/photo.jpg",
     text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
     type: "text"

     }, {
     date: new Date(),
     title: "Look at my video!",
     author: "Miranda Smith",
     profilePicture: "https://static.licdn.com/scds/common/u/images/apps/plato/home/photo_profile_headshot_200x200_v2.jpg",
     text: "Lorem ipsum dolor sit amet",
     type: "video"

     }, {
     date: new Date(),
     title: "Awesome picture",
     author: "John Mybeweeg",
     profilePicture: "http://www.lawyersweekly.com.au/images/LW_Media_Library/LW-602-p24-partner-profile.jpg",
     text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
     type: "picture"
     }]
     });

     app.controller('IconsCtrl', function($scope) {
     $scope.timeline = [{
     date: new Date(),
     title: "Awesome picture",
     author: "John Mybeweeg",
     text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
     type: "picture"
     }, {
     date: new Date(),
     title: "Look at my video!",
     author: "Miranda Smith",
     text: "Lorem ipsum dolor sit amet",
     type: "video"

     }, {
     date: new Date(),
     title: "I am here",
     author: "Ludo Anderson",
     text: "Lorem ipsum dolor sit amet",
     type: "location"

     }, {
     date: new Date(),
     title: "For my friends",
     author: "Sara Orwell",
     text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
     type: "text"

     }]
     }); */

  app.run(['$rootScope', '$ionicPlatform', function($rootScope, $ionicPlatform) {
>>>>>>> 8b5282d98a638891c5dafba3dbbeaeac75e9562b
    $ionicPlatform.ready(function() {
      $rootScope.cogo = {};
      $rootScope.cogo.trip = {};
      $rootScope.currentTrip = {};
      $rootScope.currentDest = {};
      $rootScope.currentEvent = {};
        
      //$rootScope.cogo.trip.destinations[0].events[1]
      //var d = $rootScope.cogo.trip.destinations[0];
      //var trip = $rootScope.cogo.trip;
      //trip.destination[0].....

      $rootScope.cogo.trip.destinations = [
    {
      title: 'My Dest',
    location: {
      GooglePlaceId: '',
    Address: '',
    Lat: 0,
    Long: 0
    },
    events: [
    {
      badgeClass : '',
    badgeIconClass : 'ion-ionic',
    title : 'First event',
    when : '11 hours ago via Twitter',
    content : 'Some awesome content.',
    mock: true,
    location: {
      GooglePlaceId: '',
      Address: '',
      Lat: 0,
      Long: 0
    }
    },
    {
      badgeClass : '',
      badgeIconClass : 'ion-ionic',
      title : 'First event',
      when : '11 hours ago via Twitter',
      content : 'More awesome content.',
      mock: true,
      location: {
        GooglePlaceId: '',
        Address: '',
        Lat: 0,
        Long: 0
      }
    }
    ]
    },
    {
      title: 'My Dest 2',
      location: {
        GooglePlaceId: '',
        Address: '',
        Lat: 0,
        Long: 0
      },
      events: [
      {
        badgeClass : '',
        badgeIconClass : 'ion-ionic',
        title : 'First heading',
        when : '11 hours ago via Twitter',
        content : 'Some awesome content.',
        mock: true
      }
      ]
    }

    ];

    //var trip = $rootScope.cogo.trip;
    //var currentDest =
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    });
  }]);

}());