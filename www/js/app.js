// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers', 'starter.services',
'ngMessages', 'ngSanitize', 'firebase', 'angular-md5', 'angularMoment', 'angular-timeline','angular-scroll-animate', 'starter.utils', 'starter.auth', 'ion-google-autocomplete', 'ion-datetime-picker', 'permission', 'permission.ui', 'angular-underscore' 
]);

app.run(function($ionicPlatform, Auth, $rootScope, $state, $ionicHistory, Users, Loading, PermPermissionStore, PermRoleStore) {
  $ionicPlatform.ready(function() {
      
      $rootScope.cogo = {};
      $rootScope.cogo.trip = {};
      $rootScope.currentTrip = {};
      $rootScope.currentDest = {};
      $rootScope.currentEvent = {};
        
      //$rootScope.cogo.trip.destinations[0].events[1]
      //var d = $rootScope.cogo.trip.destinations[0];
      //var trip = $rootScope.cogo.trip;
      //trip.destination[0].....
      
    $rootScope.cogo.trip.destinations = [
         {
             title: 'My Dest',
             location: {
                 GooglePlaceId: '',
                 Address: '',
                 Lat: 0,
                 Long: 0
             },
             events: [
                    {
                        badgeClass : '',
                        badgeIconClass : 'ion-ionic',
                        title : 'First event',
                        when : 'Nov 8th - 10th',
                        content : 'Some awesome content.',
                        mock: true,
                         location: {
                             GooglePlaceId: '',
                             Address: '',
                             Lat: 0,
                             Long: 0
                         }                        
                    }
             ]
         },
         {
             title: 'My Dest 2',
             location: {
                 GooglePlaceId: '',
                 Address: '',
                 Lat: 0,
                 Long: 0
             },
             events: [
                    {
                        badgeClass : '',
                        badgeIconClass : 'ion-ionic',
                        title : 'First heading',
                        when : '11 hours ago via Twitter',
                        content : 'Some awesome content.',
                        mock: true
                    }
             ]
         }
        
    ];
      
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    
    var permissions = [
        'canReadTrip',
        'canEditTrip',
        'canDeleteTrip'
    ];
    
    //var roles = [
    //    'TRIP_ADMIN', 'TRIP_VIEWER'
  //  ];
      
    PermRoleStore    
  // Or use your own function/service to validate role
    .defineManyRoles({
        'VIEWER': ['canReadTrip'],
        'ADMIN': ['canReadTrip','canEditTrip'],
        'CREATOR': ['canReadTrip','canEditTrip','canDeleteTrip']
    });
      
    PermPermissionStore.defineManyPermissions(permissions, /*@ngInject*/ function (permissionName) {
        return _.contains(permissions, permissionName);
        //var a = _.contains(permissions, permissionName);
        
    }); 
  });

  $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
    console.log(error);
    // We can catch the error thrown when the $requireSignIn promise is rejected
    // and redirect the user back to the home page
    if (error === "AUTH_REQUIRED") {
      $state.go("sign-in");
    }
  });

  $rootScope.$on('$stateChangeSuccess',function(){
      Loading.hide();
   });

  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
    Loading.show("loading...");
    // console.log("stateChangeStart");
    if((toState.name === "sign-in" || toState.name === "sign-in-with-email" ) && $rootScope.currentUser){
      event.preventDefault();
    }
  });

  Auth.$onAuthStateChanged(function(currentUser) {

    // check user auth state
    if(currentUser){
      console.log(currentUser);
      Users.setOnline(currentUser.uid);

      // check if the user add to database
      Users.get(currentUser.uid).$loaded()
      .then(function(userRecord){
        $rootScope.currentUser = userRecord;

        if(!userRecord.name || !userRecord.face || !userRecord.provider){
          Users.updateProfile(userRecord, currentUser.providerData[0])
          .then(function(ref) {
            console.log("Success Saved:", ref.key);
          }, function(error) {
            console.log("Error:", error);
          });
        }
      });

      console.log("Log In");

    }
    else {
      // logout
      $rootScope.currentUser = 0;
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
      $state.go("sign-in");
      console.log("Log Out");
    }
  });
});

app.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // auth state
  .state('sign-in', {
    url: '/sign-in',
    cache: false,
    templateUrl: 'templates/auth/sign-in.html',
    controller: 'AuthCtrl',
    resolve: {
      // controller will not be loaded until $waitForSignIn resolves
      // Auth refers to our $firebaseAuth wrapper in the example above
      "auth": ["Auth", function(Auth) {
        // $waitForSignIn returns a promise so the resolve waits for it to complete
        return Auth.$waitForSignIn();
      }]
    }
  });

  $stateProvider.state('sign-in-with-email', {
    url: '/sign-in-with-email',
    cache: false,
    templateUrl: 'templates/auth/sign-in-with-email.html',
    controller: 'AuthCtrl',
    resolve: {
      // controller will not be loaded until $waitForSignIn resolves
      // Auth refers to our $firebaseAuth wrapper in the example above
      "auth": ["Auth", function(Auth) {
        // $waitForSignIn returns a promise so the resolve waits for it to complete
        return Auth.$waitForSignIn();
      }]
    }
  });

  // setup an abstract state for the tabs directive
  $stateProvider.state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    resolve: {
      // controller will not be loaded until $requireSignIn resolves
      // Auth refers to our $firebaseAuth wrapper in the factory below
      "auth": ["Auth", function(Auth) {
        // $requireSignIn returns a promise so the resolve waits for it to complete
        // If the promise is rejected, it will throw a $stateChangeError (see above)
        return Auth.$requireSignIn();
      }]
    }
  });
    
    //Trip editor states
	$stateProvider.state('planner', {
		url: '/planner',
		templateUrl: 'templates/planner.html'
	});
    
    $stateProvider.state('timelineview',  {
            url: '/timelineview',
                    templateUrl: "templates/timelineview.html"
                    //controller: "timelineViewCtrl"
        }
    );
    
    $stateProvider.state('mapview', {
            url: '/mapview',
                    templateUrl: "templates/mapview.html"
                    //controller: "mapCtrl"
        });
	
	$stateProvider.state('ideas', {
		url: '/ideas',
		templateUrl: 'templates/ideas.html'
	});
	
	$stateProvider.state('tab.search', {
		url: '/groups/:groupId/search',
        views: {
            'tab-groups': {
                templateUrl: 'templates/search.html'
            }
        }
	});
	
	/* $stateProvider.state('settings', {
		url: '/settings',
		templateUrl: 'templates/settings.html'
	}); */
    
    $stateProvider.state('timeLine', {
    url: '/timeline',
    templateUrl: 'templates/timeline.html',
    //controller: 'timeLineCtrl'
    });

  // Each tab has its own nav history stack:
  // Tab Chats --------------------------------
  $stateProvider.state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl',
          resolve: {
            'activities': function(Activities, auth){
              return Activities.all(auth.uid).$loaded();
            }

          }

        }
      }
    });
  $stateProvider.state('tab.chat', {
    url: '/chats/:chatId',
    views: {
      'tab-chats': {
        templateUrl: 'templates/chat.html',
        controller: 'ChatCtrl',
        resolve: {
          'linkType': function(){
              return 'chats';
           },
          'chat': function(auth, Privates, Chats, $stateParams){

            return Chats.get($stateParams.chatId).$loaded(function(directChat){
              if(directChat.type){
                // navigation with chat id
                return directChat;
              }
              else{
                // use for new chat button on header bar
                // navigation to private chat
                // or create private chat relation
                return Privates.check(auth.uid, $stateParams.chatId).$loaded(function(privateChat){
                  if(privateChat.$value){
                    // navigation with user id
                    return Chats.get(privateChat.$value).$loaded();
                  }
                  else{
                    // add new private chat relation
                    return Chats.newPrivate(auth.uid, $stateParams.chatId);
                  }
                });
              }
            });

          },
          'messages': function(chat, Messages){
            return Messages.getMessages(chat.$id).$loaded();
          },
          'members': function(chat, Members){
            return Members.get(chat.$id).$loaded();
          },
          'activity': function(auth, chat, Activities, Users, $stateParams){
            return Activities.get(auth.uid, chat.$id).$loaded(function(activity){
              if(!activity.name){
                activity.name = Users.getName($stateParams.chatId);
              }
              return activity;
            });
          }
        }
      }

    }

  });
  $stateProvider.state('tab.chat-detail', {
    url: '/chats/:chatId/detail',
    views: {
      'tab-chats': {
        templateUrl: 'templates/chat-detail.html',
        controller: 'ChatDetailCtrl',
        resolve: {
          'activity': function(auth, $stateParams, Activities){
            return Activities.get(auth.uid, $stateParams.chatId).$loaded();
          },
          'members': function($stateParams, Members){
            return Members.get($stateParams.chatId).$loaded();
          }
        }

      }
    }
  });

  // Tab Groups --------------------------------
  $stateProvider.state('tab.groups', {
      url: '/groups',
      views: {
        'tab-groups': {
          templateUrl: 'templates/tab-groups.html',
          controller: 'GroupsCtrl',
          resolve: {
            'activities': function(Activities, auth){
              return Activities.group(auth.uid).$loaded();
            }
          }

        }
      }
    });
  $stateProvider.state('tab.group', {
    url: '/groups/:groupId',
    views: {
      'tab-groups': {
        templateUrl: 'templates/trip.html',
        controller: 'ChatCtrl',
        resolve: {
          'linkType': function(){
              return 'groups';
           },
          'chat': function(Chats, $stateParams){
            return Chats.get($stateParams.groupId).$loaded();
          },
          'messages': function($stateParams, Messages){
            return Messages.getMessages($stateParams.groupId).$loaded();
          },
          'members': function($stateParams, Members){
            return Members.get($stateParams.groupId).$loaded();
          },
          'activity': function(auth, $stateParams, Activities){
            return Activities.get(auth.uid, $stateParams.groupId).$loaded();
          },
          'trip': function($stateParams, Trips){
            return Trips.getTrip($stateParams.groupId).$loaded();
          }
        }
      }
    }
  });
  $stateProvider.state('tab.group-detail', {
   // url: '/groups/:groupId/detail',
    url: '/groups/:groupId/detail',
    views: {
      'tab-groups': {
          templateUrl: 'templates/chat.html',
          controller: 'ChatCtrl',
        resolve: {
          'linkType': function(){
              return 'groups';
           },
          'chat': function(Chats, $stateParams){
            return Chats.get($stateParams.groupId).$loaded();
          },
          'messages': function($stateParams, Messages){
            return Messages.getMessages($stateParams.groupId).$loaded();
          },
          'members': function($stateParams, Members){
            return Members.get($stateParams.groupId).$loaded();
          },
          'activity': function(auth, $stateParams, Activities){
            return Activities.get(auth.uid, $stateParams.groupId).$loaded();
          }
        }
          //templateUrl: 'templates/search.html'
        //templateUrl: 'templates/chat-detail.html',
        //controller: 'ChatDetailCtrl',
        //resolve: {
         // 'activity': function(auth, $stateParams, Activities){
        //    return Activities.get(auth.uid, $stateParams.groupId).$loaded();
        //  },
       //   'members': function($stateParams, Members){
        //    return Members.get($stateParams.groupId).$loaded();
       //   }
       // }
      }
    }

  });

  // Tab Contacts --------------------------------
  $stateProvider.state('friends', {
      url: '/friends',
      views: {
        'friends-list': {
          templateUrl: 'templates/friends-list.html',
          controller: 'ContactsCtrl',
          resolve: {
            'contacts': function(Users){
              return Users.all.$loaded();
            }
          }
        }
      }
    });
  $stateProvider.state('friend', {
    url: '/friends/:contactId',
    views: {
      'friends-list': {
        resolve: {
           'check': function(auth, Privates, Chats, $stateParams, $state){
             return Privates.check(auth.uid, $stateParams.contactId).$loaded(function(privateChat){

               // check if has creat private chat relation
               if(privateChat.$value){
                 // go
                 return $state.go("tab.chats").then(function(){
                   return $state.go("tab.chat",{chatId: privateChat.$value});
                 });
               }
               else{
                 // add new private chat relation
                //  then go
                 return Chats.newPrivate(auth.uid, $stateParams.contactId).then(function(chat){
                   return $state.go("tab.chats").then(function(){
                     return $state.go("tab.chat",{chatId: chat.$id});
                   });
                 });
               }
             });
           } // chat service end
        } // resolve end
      }
    }
  });
  
  $stateProvider.state('invitefriends', {
    url: '/invitefriends',
    views: {
        'ionic-modal-nav@': {
            templateUrl: 'templates/modal/invite-friends.html'
        }
    }   
  });

  // Tab Account --------------------------------
  $stateProvider.state('settings', {
    url: '/settings',
    views: {
      'tab-account': {
        templateUrl: 'templates/user-settings.html',
        controller: 'AccountCtrl'
      }
    }
  });
    
  

  // if none of the above states are matched, use this as the fallback
  //$urlRouterProvider.otherwise('/tab/groups');
  
  $urlRouterProvider.otherwise( function($injector) {
    var $state = $injector.get("$state");
    $state.go('tab.groups');
  });
    /*
    $urlRouterProvider.otherwise(function ($injector, $location) {
    return '/tab/groups';
  }); */
    
    $ionicConfigProvider.backButton.text('').icon('ion-chevron-left');

});

/* app.controller("TripStoreCtrl", function($scope, $rootScope, $firebaseArray) {
    var ref = firebase.database().ref().child("trips"); //downloading data into local object
    
    $rootScope.trips = $firebaseArray(ref);
    
    $rootScope.lastSelectedStartDate = new Date();
    $rootScope.lastSelectedEndDate = new Date();
    
   // $rootScope.lastFormattedStartDate = $rootScope.lastSelectedStartDate.toDateString();
    //$rootScope.lastFormattedEndDate = $rootScope.lastSelectedEndDate.toDateString();
   /* $scope.createTrip = function() {
             $scope.trips.$add({
                title: $scope.tripTitle,
                destinations: [
                    {
                        title: '',
                        location: $rootScope.cogo.trip.lastGMapLookupLocationResult,
                        events: [
                            {
                                badgeClass: '',
                                badgeIconClass: 'ion-ionic',
                                title: '',
                                when: '',
                                content: '',
                                location: {
                                    googlePlaceId: '',
                                    address: '',
                                    lat: 0,
                                    lng: 0
                                }
                            }
                        ]
                    }
                ]
            }).then(function(ref) {
            console.log("New trip created");
        })
        .catch(function(err) {
            throw err;
        });
    }; */
    
   /* $scope.createTrip = function() {
             return {
                title: '',
                travellers: [
                    
                ], 
                destinations: [
                    {
                        title: '',
                        location: {
                            googlePlaceId: '',
                            address: '',
                            lat: 0,
                            lng: 0
                        },
                        events: [
                            {
                                badgeClass: '',
                                badgeIconClass: 'ion-ionic',
                                title: '',
                                when: '',
                                content: '',
                                location: {
                                    googlePlaceId: '',
                                    address: '',
                                    lat: 0,
                                    lng: 0
                                }
                            }
                        ]
                    }
                ]
            };
    };
    
    $scope.$on('modal.shown', function() {

        $rootScope.newTrip = $scope.createTrip();
        console.log("New trip initialized");
    });
    
    $scope.addDestLocation = function() {
        $rootScope.newTrip.destinations[0].location.googlePlaceId = $rootScope.lastGMapLookupLocationResult.place_id;
        $rootScope.newTrip.destinations[0].location.address = $rootScope.lastGMapLookupLocationResult.formatted_address;
        $rootScope.newTrip.destinations[0].location.lat = $rootScope.lastGMapLookupLocationResult.geometry.location.lat();
        $rootScope.newTrip.destinations[0].location.lng = $rootScope.lastGMapLookupLocationResult.geometry.location.lng();
        console.log("First Destination location added");
    };
    
    $scope.addDates = function() {
        $rootScope.newTrip.destinations[0].startDate = $rootScope.lastSelectedStartDate.getTime();
        $rootScope.newTrip.destinations[0].endDate = $rootScope.lastSelectedEndDate.getTime();
        $rootScope.newTrip.destinations[0].startDateDisplay = moment($rootScope.lastSelectedStartDate).format("MMM Do");
        $rootScope.newTrip.destinations[0].endDateDisplay = moment($rootScope.lastSelectedEndDate).format("MMM Do");
    //    $rootScope.newTrip.destinations[0].startDateTimestamp = $rootScope.lastSelectedStartDate.getTime();
    //    $rootScope.newTrip.destinations[0].endDateTimestamp = $rootScope.lastSelectedEndDate.getTime();
    //    $rootScope.newTrip.destinations[0].startDateDisplay = $rootScope.lastSelectedStartDate.toString();
    //    $rootScope.newTrip.destinations[0].endDateDisplay = $rootScope.lastSelectedEndDate.toString();
    //    $rootScope.currentStartDate = new Date($rootScope.newTrip.destinations[0].startDate);
    //    $rootScope.currentEndDate = new Date($rootScope.newTrip.destinations[0].endDate);
    };
    
     /*$scope.addDate = function() {
        $rootScope.newTrip.destinations[0].location.googlePlaceId = $rootScope.lastGMapLookupLocationResult.place_id;
        $rootScope.newTrip.destinations[0].location.address = $rootScope.lastGMapLookupLocationResult.formatted_address;
        $rootScope.newTrip.destinations[0].location.lat = $rootScope.lastGMapLookupLocationResult.geometry.location.lat();
        $rootScope.newTrip.destinations[0].location.lng = $rootScope.lastGMapLookupLocationResult.geometry.location.lng();
        console.log("First Destination location added");
    };*/
    
 /*   $scope.addTrip = function() {
        $rootScope.trips.$add($rootScope.newTrip).then(function(ref) {
            console.log("New trip created");
            var id = ref.key;
            console.log("added record with id " + id);
            $rootScope.currentTrip = $rootScope.trips.$getRecord(id);
            console.log("Current trip index is " + $rootScope.trips.$indexFor(id));
        })
        .catch(function(err) {
            throw err;
        });
    };
    
}); */
    
  /*  $scope.$on('modal.shown', function() {

        $rootScope.newTrip = $scope.createTrip();
        console.log("New trip initialized");
    });
    
    $scope.addTrip = function() {
        $rootScope.trips.$add($rootScope.newTrip).then(function(ref) {
            console.log("New trip created");
        })
        .catch(function(err) {
            throw err;
        });
    }; */
    
    
  /*  $scope.createTrip = function() {
         return {
            title: '',
            destinations: [
                {
                    title: '',
                    events: [
                        {
                            badgeClass: '',
                            badgeIconClass: 'ion-ionic',
                            title: '',
                            when: '',
                            content: '',
                            location: {
                                googlePlaceId: '',
                                address: '',
                                lat: 0,
                                lng: 0
                            }
                        }
                    ]
                }
            ]
        };
    };
    
    $scope.createDest = function() {
        return {
            location: {
                        
            },
            title: '',
            events: [
                {
                    badgeClass: '',
                    badgeIconClass: 'ion-ionic',
                    title: '',
                    when: '',
                    content: '',
                    location: {
                        googlePlaceId: '',
                        address: '',
                        lat: 0,
                        lng: 0
                    }
                }
            ]
        };
    };
    
    $scope.createEvent = function() {
        return {
            badgeClass: '',
            badgeIconClass: 'ion-ionic',
            title: '',
            when: '',
            content: '',
            location: {
                googlePlaceId: '',
                address: '',
                lat: 0,
                lng: 0
            }
        };
    };
    
    $scope.$on('modal.shown', function() {
        
    
        var newTrip = $scope.createTrip();
   // $rootScope.dest = $scope.createDest();
   // $rootScope.event = $scope.createEvent();
    
        $rootScope.trips.$add(newTrip).then(function(ref) {
            var id = ref.key;
            console.log("added record with id " + id);
            $rootScope.currentTrip = $rootScope.trips.$getRecord(id);
            console.log("Current trip index is " + $rootScope.trips.$indexFor(id));
        }).catch(function(err) {
            throw err;  
        });
    });
    
   $scope.saveTrip = function() {
        
        $rootScope.currentTrip.destinations[0].location = 
            {
                 googlePlaceId: $rootScope.cogo.trip.lastGMapLookupLocationResult.place_id,
                 address: $rootScope.cogo.trip.lastGMapLookupLocationResult.formatted_address,
                 lat: $rootScope.cogo.trip.lastGMapLookupLocationResult.geometry.location.lat(),
                 lng: $rootScope.cogo.trip.lastGMapLookupLocationResult.geometry.location.lng()
		    };
        
        $rootScope.trips.$save($rootScope.currentTrip).then(function(ref) {
            
            console.log("Trip saved");
        })
        .catch(function(err) {
            throw err;
        });
    };
    
    $scope.deleteTrip = function() {
        $rootScope.trips.$remove($rootScope.currentTrip).then(function(ref) {
            console.log("New trip canceled");
        })
        .catch(function(err) {
            throw err;
        });
    }; */
    
    
    //....trip.destinations.$add($rootScope.dest)
    //....trip.dest.events.$add($rootScope.event)
    
    //<input type='text' ng-model='trip.title' name="Title"></input>
    //<input type='text' ng-model='trip.destinations[trip.cdi].location.lat' name="lat"></input>
    //....firebase....$save($rootScope.trip);
    
    // Creating sync-ed array
    /* $rootScope.trips = $firebaseArray(ref);
    // Adding new items to array
    $scope.addTrip = function() {
        $scope.trips.$add({
            title: '',
            destinations: [
                {
                    location: {
                         
                    },
                    title: '',
                    events: [
                        {
                            badgeClass : '',
                            badgeIconClass : 'ion-ionic',
                            title : '',
                            when : '',
                            content : '',
                             location: {
                                 GooglePlaceId: '',
                                 Address: '',
                                 Lat: 0,
                                 Long: 0
                             }                        
                        }
                    ]
                }
                
            ]
            
        })
        
    }; 
    
}); */


app.controller('TripMenuCtrl', ['$scope', function($scope) {
    
    $scope.messenger =
        { name: 'messenger', url: 'templates/chat.html'};
    
    $scope.templates =
        [{ name: 'Itinerary', url: 'templates/timelineview.html'},
         { name: 'Ideas', url: 'templates/ideas.html'},
         { name: 'Search', url: 'templates/search.html'},
         { name: 'Settings', url: 'templates/settings.html'}];
    //$scope.pos = 0;
    $scope.template = $scope.templates[0];
    $scope.planner = function() {
        $scope.template = $scope.templates[0];
    };
    $scope.ideas = function() {
        $scope.template = $scope.templates[1];
    };
    $scope.search = function() {
        $scope.template = $scope.templates[2];
    };
    $scope.settings = function() {
        $scope.template = $scope.templates[3];
    };
}]);

app.controller('messengerCtrl', ['$scope', function($scope) {
    
    $scope.template =
        { name: 'messenger', url: 'templates/chat.html'};
}]);

// Toggle Timeline and Map View
app.controller('PlannerCtrl', ['$scope', function($scope) {
    
    $scope.templates =
        [{ name: 'timelineview', url: 'templates/timelineview.html'},
         { name: 'map', url: 'templates/mapview.html'}];
    $scope.buttons = 
        [{name: 'map', icon: 'ion-map'},
         {name: 'timeline', icon: 'ion-calendar'}];
    $scope.pos = 0;
    $scope.template = $scope.templates[$scope.pos];
    $scope.button = $scope.buttons[$scope.pos];
    $scope.toggleView = function() {
        /*if($scope.template.url = 'templates/timelineview.html')
            { $scope.template = $scope.templates[1];
            }
        else if($scope.template.url = 'templates/mapview.html') {
            $scope.template = $scope.templates[0];
        } */
        $scope.pos = ($scope.pos + 1)% 2;
        $scope.template = $scope.templates[$scope.pos];
        $scope.button = $scope.buttons[$scope.pos];
    };
}]);

// Map View Controller
    
app.controller('MapCtrl', function($scope, $rootScope, $state, $cordovaGeolocation) {
  var options = {timeout: 10000, enableHighAccuracy: true};
 
 /* $cordovaGeolocation.getCurrentPosition(options).then(function(position){
 
    var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
 
    var mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
 
      
      $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
  
  google.maps.event.trigger($scope.map, "resize");
    //Wait until the map is loaded
  google.maps.event.addListenerOnce($scope.map, 'idle', function() {
      
      var events = $rootScope.cogo.trip.destinations[0].events;
      
      for (var i=0;i<events.length;i++) {
          var event = events[i];
          var latlong = new google.maps.LatLng(event.location.lat, event.location.lng);
          var marker = new google.maps.Marker({
              map: $scope.map,
              animation: google.maps.Animation.DROP,
              position: latlong
              });
      
          var infoWindow = new google.maps.InfoWindow({
              content: "This is your location"
          });
      
          google.maps.event.addListener(marker, 'click', function () {
          infoWindow.open($scope.map, marker);
          });
          
      }
 
 });
      
      
  }, function(error){
    console.log("Could not get location");
  }); */
    
    $cordovaGeolocation.getCurrentPosition(options).then(function(position){
 
        var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        var mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          fullscreenControl: false
        };
 
      
        $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
  
      google.maps.event.trigger($scope.map, "resize");
        //Wait until the map is loaded
      google.maps.event.addListenerOnce($scope.map, 'idle', function() {

         //var events = $rootScope.cogo.trip.destinations[0].events;
         var dests = $rootScope.currentTrip.destinations;
         var bounds = new google.maps.LatLngBounds();

          for ( var i=0; i<dests.length; i++ ) {
              var dest = dests[i];
              if (dest.mock) {
                  continue;
              }
              var latlong = new google.maps.LatLng(dest.location.lat, dest.location.lng);
              bounds.extend(latlong);
              var marker = new google.maps.Marker({
                  map: $scope.map,
                  animation: google.maps.Animation.DROP,
                  position: latlong,
                  content: dest.location.address
                  });

              var infoWindow = new google.maps.InfoWindow({
              });

              google.maps.event.addListener(marker, 'click', function (e) {
                        //infoWindow.setContent(this.position);
                        infoWindow.setContent(this.content);
                        infoWindow.open(this.get('map'), this);
              });
              
              // add the double-click event listener
                google.maps.event.addListener(marker, 'dblclick', function(event){
                    map = marker.getMap();    
                    map.setCenter(marker.getPosition()); // set map center to marker position
                    $scope.smoothZoom(map, 12, map.getZoom()); // call smoothZoom, parameters map, final zoomLevel, and starting zoom level
                });
              
                // the smooth zoom function
              $scope.smoothZoom =  function(map, max, cnt) {
                    if (cnt >= max) {
                        return;
                    }
                    else {
                        z = google.maps.event.addListener(map, 'zoom_changed', function(event){
                            google.maps.event.removeListener(z);
                            $scope.smoothZoom(map, max, cnt + 1);
                        });
                        setTimeout(function(){map.setZoom(cnt)}, 80); // 80ms is what I found to work well on my system -- it might not work well on all systems
                    }
                };
              
               /* marker.addListener('click', function() {
                    infowindow.open(marker.get('map'), marker);
              }); */
          }

          $scope.map.fitBounds(bounds);
      });   
      
      }, function(error){
        console.log("Could not get location");
  }); 
});

app.controller('addPlanCtrl', function($scope, $ionicModal, $rootScope) {
  
        $ionicModal.fromTemplateUrl('templates/add-plan.html', {
        scope: $scope,
        animation: 'slide-in-up'
          }).then(function(modal) {
            $scope.modal = modal;
          });
    
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
});

app.controller('addDestCtrl', function($scope, $ionicModal, $rootScope) {
  
        $ionicModal.fromTemplateUrl('templates/add-dest.html', {
        scope: $scope,
        animation: 'slide-in-up'
          }).then(function(modal) {
            $scope.modal = modal;
          });
    
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
});

app.controller('gMapsAutoFillCtrl', function ($scope, $rootScope) {

    $scope.data = {};

    //Optional - change countryCode to 'cities' to restrict to cities, change to 'all' to remove restriction
    $scope.countryCode = 'cities';
   // $scope.opts = 'cities' ;

    //Optional
    $scope.onAddressSelection = function (location) {
        $rootScope.lastGMapLookupLocationResult = location;
        console.log($rootScope.lastGMapLookupLocationResult);
        //Do something
        //alert(location.formatted_address);
    };
    
});

app.controller('addTripCtrl', ['$scope', function($scope) {
    
    $scope.templates =
        [{ name: 'newtripdest', url: 'templates/newtrip-dest.html'},
         { name: 'newtripinvite', url: 'templates/newtrip-invite.html'}];
    $scope.pos = 0;
    $scope.template = $scope.templates[$scope.pos];
    $scope.nextTripStep = function() {
        $scope.pos = $scope.pos + 1;
        $scope.template = $scope.templates[$scope.pos];
    };
    $scope.prevTripStep = function() {
        $scope.pos = $scope.pos - 1;
        $scope.template = $scope.templates[$scope.pos];
    };
    
}]);

app.controller('timeLineCtrl', function($rootScope, $document, $timeout, $scope, $ionicScrollDelegate, Trips) {
    /*var ref = firebase.database().ref().child("trips"); //downloading data into local object
    
    $rootScope.trips = $firebaseArray(ref);*/

	//})

	//var ExampleCtrl = function($rootScope, $document, $timeout, $scope) {
    
	$scope.side = '';
    
    
    
    $scope.dest = 
        {
                        badgeClass: '',
                        badgeIconClass: 'ion-ionic',
                        title: '',
                        when: '',
                        location: {
                            googlePlaceId: '',
                            address: '',
                            lat: 0,
                            lng: 0
                        },
                        plans: [
                            {
                                badgeClass: '',
                                badgeIconClass: 'ion-ionic',
                                title: '',
                                when: 'when',
                                content: '',
                                location: {
                                    googlePlaceId: '',
                                    address: '',
                                    lat: 0,
                                    lng: 0
                                }
                            }
                        ]
        };
    
    $scope.plan = {
        badgeClass: '',
        badgeIconClass: 'ion-ionic',
        title: '',
        type: '',
        when: 'when'
    };
    
    $scope.createDest = function() {
        return {
                        badgeClass: '',
                        badgeIconClass: 'ion-ionic',
                        title: '',
                        when: 'when',
                        location: {
                            googlePlaceId: '',
                            address: '',
                            lat: 0,
                            lng: 0
                        },
                        plans: [
                            {
                                badgeClass: '',
                                badgeIconClass: 'ion-ionic',
                                title: '',
                                when: 'when',
                                content: '',
                                location: {
                                    googlePlaceId: '',
                                    address: '',
                                    lat: 0,
                                    lng: 0
                                }
                            }
                        ]
        };
    };
    
    $scope.createPlan = function() {
        return {
            badgeClass: '',
            badgeIconClass: 'ion-ionic',
            title: '',
            type: '',
            when: 'when',
            location: {
                googlePlaceId: '',
                            address: '',
                            lat: 0,
                            lng: 0
            }
        };
    };

	/*$scope.events = [{
		badgeClass : '',
		badgeIconClass : 'ion-ionic',
		title : 'First heading',
		when : '11 hours ago via Twitter',
		content : 'Some awesome content.'
	}, {
		badgeClass : 'bg-positive',
		badgeIconClass : 'ion-gear-b',
		title : 'Second heading',
		when : '12 hours ago via Twitter',
		content : 'More awesome content.'
	}, {
		badgeClass : 'bg-balanced',
		badgeIconClass : 'ion-person',
		title : 'Third heading',
		titleContentHtml : '<img class="img-responsive" src="http://www.freeimages.com/assets/183333/1833326510/wood-weel-1444183-m.jpg">',
		contentHtml : lorem,
		footerContentHtml : '<a href="">Continue Reading</a>'
	}]; */
    
    $scope.$on('modal.shown', function() {
        
        $scope.newDest = $scope.createDest();
        $scope.newPlan = $scope.createPlan();
        //$scope.newDests = [];
        $scope.newDest.startDate;
        $scope.newDest.endDate;
        $scope.newPlan.startDate;
        $scope.newPlan.endDate;
        console.log("New dest/plan initialized");
    });
    

	$scope.newDestination = function() {
        
        
        
        /*$scope.newDests.push({
                        badgeClass: '',
                        badgeIconClass: 'ion-ionic',
                        title: '',
                        when: '',
                        location: {
                            googlePlaceId: '',
                            address: '',
                            lat: 0,
                            lng: 0
                        },
                        events: [
                            {
                                badgeClass: '',
                                badgeIconClass: 'ion-ionic',
                                title: '',
                                when: '',
                                content: '',
                                location: {
                                    googlePlaceId: '',
                                    address: '',
                                    lat: 0,
                                    lng: 0
                                }
                            }
                        ]
        });*/
        
        /*$scope.dest.location = 
            {
                 googlePlaceId: $rootScope.lastGMapLookupLocationResult.place_id,
                 address: $rootScope.lastGMapLookupLocationResult.formatted_address,
                 lat: $rootScope.lastGMapLookupLocationResult.geometry.location.lat(),
                 lng: $rootScope.lastGMapLookupLocationResult.geometry.location.lng()
		    };
        
        $scope.dest.startDateStamp = $scope.startDate.getTime();
        $scope.dest.endDateStamp = $scope.endDate.getTime();
        $scope.dest.startDateDisplay = moment($scope.startDate).format("MMM Do");
        $scope.dest.endDateDisplay = moment($scope.endDate).format("MMM Do");
        
		$rootScope.currentTrip.destinations.push($scope.dest);
        $rootScope.trips.$save($rootScope.currentTrip);
		$ionicScrollDelegate.resize();
        $scope.dest = {};*/
        
            
        /*    {
			badgeClass : 'bg-royal',
			badgeIconClass : 'ion-checkmark',
			title : 'Fourth event',
			when : '3 hours ago via Twitter',
			content : 'Some awesome content.',
            location: {
                 googlePlaceId: $rootScope.cogo.trip.lastGMapLookupLocationResult.place_id,
                 address: $rootScope.cogo.trip.lastGMapLookupLocationResult.formatted_address,
                 lat: $rootScope.cogo.trip.lastGMapLookupLocationResult.geometry.location.lat(),
                 lng: $rootScope.cogo.trip.lastGMapLookupLocationResult.geometry.location.lng()
             }
		} */
       
	};
    
    $scope.addDestLocation = function() {
        $scope.newDest.location.googlePlaceId = $rootScope.lastGMapLookupLocationResult.place_id;
        $scope.newDest.location.address = $rootScope.lastGMapLookupLocationResult.formatted_address;
        $scope.newDest.location.lat = $rootScope.lastGMapLookupLocationResult.geometry.location.lat();
        $scope.newDest.location.lng = $rootScope.lastGMapLookupLocationResult.geometry.location.lng();
        console.log("First Destination location added");
        $rootScope.lastGMapLookupLocationResult = {};
    };
    
    $scope.addDates = function() {
        $scope.newDest.startDateStamp = $scope.newDest.startDate.getTime();
        $scope.newDest.endDateStamp = $scope.newDest.endDate.getTime();
        $scope.newDest.startDateDisplay = moment($scope.newDest.startDate).format("MMM Do");
        $scope.newDest.endDateDisplay = moment($scope.newDest.endDate).format("MMM Do");
    //    $rootScope.newTrip.destinations[0].startDateTimestamp = $rootScope.lastSelectedStartDate.getTime();
    //    $rootScope.newTrip.destinations[0].endDateTimestamp = $rootScope.lastSelectedEndDate.getTime();
    //    $rootScope.newTrip.destinations[0].startDateDisplay = $rootScope.lastSelectedStartDate.toString();
    //    $rootScope.newTrip.destinations[0].endDateDisplay = $rootScope.lastSelectedEndDate.toString();
    //    $rootScope.currentStartDate = new Date($rootScope.newTrip.destinations[0].startDate);
    //    $rootScope.currentEndDate = new Date($rootScope.newTrip.destinations[0].endDate);
    };
    
    $scope.addPlanInfo = function() {
        $scope.newPlan.location.googlePlaceId = $rootScope.lastGMapLookupLocationResult.place_id;
        $scope.newPlan.location.address = $rootScope.lastGMapLookupLocationResult.formatted_address;
        $scope.newPlan.location.lat = $rootScope.lastGMapLookupLocationResult.geometry.location.lat();
        $scope.newPlan.location.lng = $rootScope.lastGMapLookupLocationResult.geometry.location.lng();
        console.log("First Destination location added");
        $rootScope.lastGMapLookupLocationResult = {};
        
        $scope.newPlan.startDateStamp = $scope.newPlan.startDate.getTime();
        $scope.newPlan.endDateStamp = $scope.newPlan.endDate.getTime();
        $scope.newPlan.startDateDisplay = moment($scope.newPlan.startDate).format("MMM Do");
        $scope.newPlan.endDateDisplay = moment($scope.newPlan.endDate).format("MMM Do");
    };
    
    $scope.addNewDestLocation = function(dest) {
        dest.location = 
            {
                 googlePlaceId: $rootScope.lastGMapLookupLocationResult.place_id,
                 address: $rootScope.lastGMapLookupLocationResult.formatted_address,
                 lat: $rootScope.lastGMapLookupLocationResult.geometry.location.lat(),
                 lng: $rootScope.lastGMapLookupLocationResult.geometry.location.lng()
		    };
    };
    
    $scope.addNewDestStartDate = function(dest) {
        //dest.startDateStamp = $scope.startDate.getTime();
        dest.startDateDisplay = moment($scope.startDate).format("MMM Do");
    };
    
    $scope.addNewDestEndDate = function(dest) {
        //dest.endDateStamp = $scope.endDate.getTime();
        dest.endDateDisplay = moment($scope.endDate).format("MMM Do");
    };
    
    $scope.saveNewDests = function() {
        //Trips.addDestinations($rootScope.currentTrip.$id, $scope.newDests);
        Trips.addDestinations($rootScope.currentTrip.$id, $scope.newDest);
    };
    
    $scope.saveNewPlans = function() {
        //Trips.addDestinations($rootScope.currentTrip.$id, $scope.newDests);
        Trips.addPlans($rootScope.currentTrip.$id, $rootScope.currentDestIndex, $scope.newPlan);
    };
    
    
	// optional: not mandatory (uses angular-scroll-animate)
	$scope.animateElementIn = function($el) {
		$el.removeClass('timeline-hidden');
		$el.addClass('bounce-in');
	};

	// optional: not mandatory (uses angular-scroll-animate)
	$scope.animateElementOut = function($el) {
		$el.addClass('timeline-hidden');
		$el.removeClass('bounce-in');
	};
	
	$scope.reExecuteAnimation = function(){
		TM = document.getElementsByClassName('tm');
		for (var i = 0; i < TM.length; i++) {
	        removeAddClass(TM[i], 'bounce-in');
	    }
	}
	
/*	$scope.rePerformAnimation = function(){
	   $scope.reExecuteAnimation();
	}

	$scope.leftAlign = function() {
		$scope.side = 'left';
		$ionicScrollDelegate.resize();
		
		$scope.reExecuteAnimation();
	}

	$scope.rightAlign = function() {
		$scope.side = 'right';
		$ionicScrollDelegate.resize();
		
		$scope.reExecuteAnimation();
	}

	$scope.defaultAlign = function() {
		$scope.side = '';
		$ionicScrollDelegate.resize();
		
		$scope.reExecuteAnimation();
		// or 'alternate'
	} */
    
    /*if($rootScope.currentDest !== null) {
        $rootScope.timelineView = $rootScope.timelineViews[1];
    }
    else {
        $rootScope.timelineView = $rootScope.timelineViews[0];
    }*/
    
    $rootScope.selectedDest = {value: 0};
    
    $scope.goToDestView = function(){
        $rootScope.timelineTemplate = $rootScope.tripTemplates[$scope.pos].timelineView[1];
        $rootScope.currentDest = $rootScope.currentTrip.destinations[$rootScope.selectedDest.value];
        $rootScope.currentDestIndex = $rootScope.selectedDest.value;
        //$rootScope.currentDestStatus = true;
    };
    
    $rootScope.goToTripView = function(){
        //$rootScope.timelineView = $rootScope.timelineViews[0];
        $rootScope.timelineTemplate = $rootScope.tripTemplates[$scope.pos].timelineView[0];
        $rootScope.selectedDest.value = 0;
        $rootScope.currentDest = null;
        $rootScope.currentDestIndex = null;
        //$rootScope.currentDestStatus = false;
    };
    
    
});

app.controller('fullTimelineCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {
    
    /*if($rootScope.currentDest !== null) {
        
    }*/
    
    $rootScope.currentDest = null;
    $rootScope.currentDestIndex = null;
    //$rootScope.currentDestStatus = false;
    
    $rootScope.tripTemplates =
        [{ timelineView: [{ name: 'wholetripview', url: 'templates/wholetripview.html'},
         { name: 'destination', url: 'templates/destview.html'}]},
         { name: 'map', url: 'templates/mapview.html'}];
    $scope.tripButtons = 
        [{name: 'map', icon: 'ion-map'},
         {name: 'timeline', icon: 'ion-calendar'}];
    $scope.pos = 0;
    //$scope.template = $scope.tripTemplates[$scope.pos];
    $rootScope.timelineTemplate = $rootScope.tripTemplates[$scope.pos].timelineView[0];
    $scope.button = $scope.tripButtons[$scope.pos];
    $scope.toggleView = function() {
        /*if($scope.template.url = 'templates/timelineview.html')
            { $scope.template = $scope.templates[1];
            }
        else if($scope.template.url = 'templates/mapview.html') {
            $scope.template = $scope.templates[0];
        } */
        $scope.pos = ($scope.pos + 1)% 2;
        //$scope.template = $scope.tripTemplates[$scope.pos];
        $scope.button = $scope.tripButtons[$scope.pos];
            
        if($scope.pos == 0 && $rootScope.currentDest == null) {
            $rootScope.timelineTemplate = $rootScope.tripTemplates[$scope.pos].timelineView[0];
        }
        else if($scope.pos == 0 && $rootScope.currentDest !== null) {
            $rootScope.timelineTemplate = $rootScope.tripTemplates[$scope.pos].timelineView[1];
        }
        else {
            $rootScope.timelineTemplate = $rootScope.tripTemplates[$scope.pos];
        }
    };
    
    $rootScope.timelineViews =
        [{ name: 'wholetripview', url: 'templates/wholetripview.html'},
         { name: 'destination', url: 'templates/destview.html'}];
    
    $scope.pos = 0;
    $rootScope.timelineView = $rootScope.timelineViews[$scope.pos];
}]);

app.directive('groupedRadio', function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    scope: {
      model: '=ngModel',
      value: '=groupedRadio'
    },
    link: function(scope, element, attrs, ngModelCtrl) {
      element.addClass('button');
      element.on('click', function(e) {
        scope.$apply(function() {
          ngModelCtrl.$setViewValue(scope.value);
        });
      });

      scope.$watch('model', function(newVal) {
        element.removeClass('button-balanced');
        if (newVal === scope.value) {
          element.addClass('button-balanced');
        }
      });
    }
  };
})

app.controller('RadioCtrl', function($scope, $rootScope) {
    
    $rootScope.selectedType = null;
    $rootScope.transport = 'transportation';
    $rootScope.stay = 'stay';
    $rootScope.event = 'event';
    $rootScope.transportSelect - function() {
        $rootScope.selectedType = 1;
    };
    $rootScope.staySelect - function() {
        $rootScope.selectedType = 2;
    };
    $rootScope.eventSelect - function() {
        $rootScope.selectedType = 3;
    };
    
  $scope.data = {
    transportationType: ''
  };
});