angular.module('starter.controllers', [])
.controller('ChatsCtrl', function($scope, Activities, ModalService, Users, activities) {

  // Tab Chats show last Activities
  $scope.activities = activities;

  $scope.remove = function(chat) {
    // Activities.remove(chat);
  };

  // Top right button that create new private chat
  $scope.newChat = function() {
    ModalService
      .init('templates/modal/new-chat.html', $scope)
      .then(function(modal) {
        modal.show();
        Users.all.$loaded(function(users){
          $scope.users = users;
        });
        $scope.linkType = $scope.users;
      });
  };
})

.controller('ChatCtrl', function($scope, $stateParams, $rootScope,
  Activities, Messages, Users, Chats,
  chat, messages, Members, members, activity,
  Loading, linkType, Trips, trip, $state) {
    
  // link of chat-detail
  $scope.linkType = linkType;

  // message list and chat name and trip content
  $scope.messages = messages;
  $scope.activity = activity;
  $rootScope.currentTrip = trip;
  $scope.chat = chat;
  //var tripId = $scope.activity.tripId;
    
        //$rootScope.currentTrip = Trips.getTrip(tripId);

 
 //$rootScope.currentTripId = activity.tripId;
  console.log("Current trip is: " + $rootScope.currentTrip.$id);
  console.log("Current trip is: " + $rootScope.currentTrip.title);
  // chat members
  
    $scope.members = members;

  // user send new message
  $scope.sendChat = function(chatText){

    // message obj
    var message = {
      userId: $rootScope.currentUser.$id,
      name: $rootScope.currentUser.name,
      face: $rootScope.currentUser.face,
      message: chatText,
      timestamp: firebase.database.ServerValue.TIMESTAMP
    };

    // send Message
    Messages.addMessageAndUpdateActivities($scope.activity.$id, message, $scope.members);

  }
  
  $scope.deleteThisTrip = function(){
      Trips.deleteTrip($rootScope.currentTrip.$id, $rootScope.currentUser.$id);
      Activities.removeActivity(/*$rootScope.currentUser.$id, */$scope.chat.$id, $scope.members);
      Members.removeAllMembers($scope.chat.$id);
      Chats.removeChat($scope.chat.$id);
      $state.go("tab.groups");
  }
    
  $scope.leaveThisTrip = function(){
      Trips.leaveTrip($rootScope.currentTrip.$id, $rootScope.currentUser.$id);
      Activities.leaveActivity($rootScope.currentUser.$id, $scope.chat.$id);
      Members.removeMember($scope.chat.$id, $rootScope.currentTrip.$id);
  }


})

.controller('ChatDetailCtrl', function($scope, $stateParams, Activities, Members, ModalService, Users, $filter, $ionicHistory, toggleSelection,
activity, members, Chats, Privates, $state, $rootScope) {

  // get activity
  $scope.activity = activity;

  // convert activity.face string to thumbs array
  if($scope.activity.face){
    $scope.thumbs = $filter('split')($scope.activity.face);
  }

  // chatMember store current chat member list
  $scope.chatMember = [];
  $scope.members = members;
  for(var i=0; i<$scope.members.length; i++){
    $scope.chatMember.push(Users.get($scope.members[i].$id));
  }

  $scope.newGroup = function() {
    ModalService
      .init('templates/modal/new-group.html', $scope)
      .then(function(modal) {
        modal.show();

        // init new group modal data
        $scope.modalView = {};
        $scope.modalView.groupMember = $scope.activity.members;
        $scope.modalView.groupName = $scope.activity.name;
        $scope.modalView.groupFace = $scope.activity.face;

        // get user list
        Users.all.$loaded(function(contacts){

          // user list
          $scope.contacts = contacts;
          for(var i=0; i<$scope.contacts.length; i++){
            $scope.contacts[i].checked = null;
          }

          // set current chat member checked
          // selectedMember store checkbox member select
          $scope.selectedMember = [];
          for(var i=0; i<$scope.members.length; i++){
            for(var j=0; j<$scope.contacts.length; j++){
              if($scope.members[i].$id == $scope.contacts[j].$id){
                $scope.contacts[j].checked = true;
                $scope.selectedMember.push($scope.contacts[j]);
              }
            }
          }

        });


      });
  };

  $scope.toggleSelection = function(obj){
    return toggleSelection(obj, $scope.selectedMember, $scope.modalView);
  }

  $scope.createNewGroup = function(){
    // must 3 people can create group
    if($scope.selectedMember.length > 2){
      var name, lastText, face, members, chatType;

      // if multiple member select,
      // make name, face and members data
      // based on these members info
      if(Array.isArray($scope.modalView.groupName)){
        name = $scope.modalView.groupName.join(", ");
      }
      else {
          name = $scope.modalView.groupName;
      }

      if(Array.isArray($scope.modalView.groupFace)){
        face = $scope.modalView.groupFace.join(", ");
      }
      else {
          face = $scope.modalView.groupFace;
      }

      if(Array.isArray($scope.modalView.groupMember)){
        members = $scope.modalView.groupMember.join(", ");
      }
      else {
          members = $scope.modalView.groupMember;
      }

      lastText = $rootScope.currentUser.name + " created the group";
      chatType = "group";
      Chats.update($scope.activity.$id, chatType);

      // remove private reference
      if($scope.members.length == 2){
        Privates.remove($scope.members[0].$id, $scope.members[1].$id);
        Privates.remove($scope.members[1].$id, $scope.members[0].$id);
      }


      // update Activities services
      for(var i=0; i<$scope.selectedMember.length; i++){
        Activities.add($scope.selectedMember[i].$id, $scope.activity.$id, name, lastText, face, members, chatType);
      }

      Activities.get($rootScope.currentUser.$id, $scope.activity.$id).$loaded(function(activities){
        $scope.activity = activities;
      });

      $scope.thumbs = $filter('split')(face);

      // update current chat members list
      $scope.chatMember = $scope.selectedMember;

      // update members service
      var memberIdList = [];
      for(var i=0; i<$scope.selectedMember.length; i++){
        memberIdList.push($scope.selectedMember[i].$id);
        Members.addMember($scope.activity.$id, $scope.selectedMember[i].$id);
      }

      // close
      $scope.closeModal();
      $ionicHistory.clearCache();

    }
  }
})

.controller('GroupsCtrl', function(Chats, $scope, Activities, Users, ModalService, $filter, Members, toggleSelection, $state, $ionicHistory, Messages, activities, $rootScope, $ionicModal, $firebaseArray, Trips) {
  //var ref = firebase.database().ref().child("trips"); //downloading data into local object
    
    //$rootScope.trips = $firebaseArray(ref);
    
    
  // get group list
  $scope.groups = activities;
  
    $rootScope.currentDest = null;

  $scope.new_group = function() {
    ModalService
      .init('templates/modal/new-group.html', $scope)
      .then(function(modal) {
        modal.show();

        // init new group modal data
        $scope.modalView = {};

        // get user list
        Users.all.$loaded(function(contacts){

          $scope.contacts = contacts;
          for(var i=0; i<$scope.contacts.length; i++){
            $scope.contacts[i].checked = null;
          }

          // set current chat member checked
          // selectedMember store checkbox member select
          $scope.selectedMember = [];
          Users.get($rootScope.currentUser.$id).$loaded(function(me){
            $scope.selectedMember.push(me);
          });

          console.log($scope.selectedMember);

        });

      });
  };


  $scope.toggleSelection = function(obj){
    return toggleSelection(obj, $scope.selectedMember, $scope.modalView);
  }
  
  
  $scope.createTrip = function() {
             return {
                title: '', 
                destinations: [
                    {
                        badgeClass: '',
                        badgeIconClass: 'ion-ionic',
                        title: '',
                        when: 'when',
                        location: {
                            googlePlaceId: '',
                            address: '',
                            lat: 0,
                            lng: 0
                        },
                        plans: [
                            {
                                badgeClass: '',
                                badgeIconClass: 'ion-ionic',
                                title: '',
                                when: '',
                                content: '',
                                location: {
                                    googlePlaceId: '',
                                    address: '',
                                    lat: 0,
                                    lng: 0
                                }
                            }
                        ]
                    }
                ]
            };
    };
    
    $scope.$on('modal.shown', function() {

        $rootScope.newTrip = $scope.createTrip();
        console.log("New trip initialized");
        $rootScope.newTrip.destinations[0].startDate;
        $rootScope.newTrip.destinations[0].endDate;
    });
    
    
    $scope.addDestLocation = function() {
        $rootScope.newTrip.destinations[0].location.googlePlaceId = $rootScope.lastGMapLookupLocationResult.place_id;
        $rootScope.newTrip.destinations[0].location.address = $rootScope.lastGMapLookupLocationResult.formatted_address;
        $rootScope.newTrip.destinations[0].location.lat = $rootScope.lastGMapLookupLocationResult.geometry.location.lat();
        $rootScope.newTrip.destinations[0].location.lng = $rootScope.lastGMapLookupLocationResult.geometry.location.lng();
        console.log("First Destination location added");
        $rootScope.lastGMapLookupLocationResult = {};
    };
    
    $scope.addDates = function() {
        $rootScope.newTrip.destinations[0].startDateStamp = $rootScope.newTrip.destinations[0].startDate.getTime();
        $rootScope.newTrip.destinations[0].endDateStamp = $rootScope.newTrip.destinations[0].endDate.getTime();
        $rootScope.newTrip.destinations[0].startDateDisplay = moment($rootScope.newTrip.destinations[0].startDate).format("MMM Do");
        $rootScope.newTrip.destinations[0].endDateDisplay = moment($rootScope.newTrip.destinations[0].endDate).format("MMM Do");
    //    $rootScope.newTrip.destinations[0].startDateTimestamp = $rootScope.lastSelectedStartDate.getTime();
    //    $rootScope.newTrip.destinations[0].endDateTimestamp = $rootScope.lastSelectedEndDate.getTime();
    //    $rootScope.newTrip.destinations[0].startDateDisplay = $rootScope.lastSelectedStartDate.toString();
    //    $rootScope.newTrip.destinations[0].endDateDisplay = $rootScope.lastSelectedEndDate.toString();
    //    $rootScope.currentStartDate = new Date($rootScope.newTrip.destinations[0].startDate);
    //    $rootScope.currentEndDate = new Date($rootScope.newTrip.destinations[0].endDate);
    };
    
    
     /* $scope.addTravellers = function() {
          for(var i=0; i<$scope.selectedMember.length; i++){
          $rootScope.newTrip.travellers.$add({travellerId: $scope.selectedMember[i].$id});
        }
          
      }; */
    
    
    
    /*$scope.addNewTrip = function() {
        $rootScope.trips.$add($rootScope.newTrip).then(function(ref) {
            console.log("New trip created");
            var id = ref.key;
            console.log("added record with id " + id);
            $rootScope.createdTripId = id;
            $rootScope.createdTrip = $rootScope.trips.$getRecord(id);
            console.log("Current trip index is " + $rootScope.trips.$indexFor(id));
            $rootScope.createdTrip.travellers = [];
            for(var i=0; i<$scope.selectedMember.length; i++){
                if ($scope.selectedMember[i].$id === $scope.currentUser.$id) {
                    $rootScope.createdTrip.travellers.push({travellerId: $scope.selectedMember[i].$id})
                }
              $rootScope.createdTrip.travellers.push({travellerId: $scope.selectedMember[i].$id});
            }
            $rootScope.trips.$save($rootScope.createdTrip);
        })
        .catch(function(err) {
            throw err;
        });
    };*/
  

  $scope.createNewGroup = function(){
    // must 3 people can create group
    if($scope.selectedMember.length > 2){
      var name, lastText, face, members, chatType, tripId, tripTitle;

      // if multiple member select,
      // make name, face and members data
      // based on these members info
      if(Array.isArray($scope.modalView.groupName)){
        name = $scope.modalView.groupName.join(", ");
      }
      else {
          name = $scope.modalView.groupName;
      }

      if(Array.isArray($scope.modalView.groupFace)){
        face = $scope.modalView.groupFace.join(", ");
      }
      else {
          face = $scope.modalView.groupFace;
      }

      if(Array.isArray($scope.modalView.groupMember)){
        members = $scope.modalView.groupMember.join(", ");
      }
      else {
          members = $scope.modalView.groupMember;
      }

      lastText = $rootScope.currentUser.name + " created the group";
      chatType = "group";
      //tripId = $rootScope.createdTripId;
      
     // $scope.addTravellers = function() {
          
          
    //  }
        
        
      Chats.newGroup($scope.selectedMember).then(function(chat){
        
        tripTitle = $rootScope.newTrip.title;  
        Trips.addTrip($rootScope.currentUser.$id, chat.$id, $scope.selectedMember, $rootScope.newTrip.title, $rootScope.newTrip.destinations);
        tripId = chat.$id;

        // update Activities services
        for(var i=0; i<$scope.selectedMember.length; i++){
          Activities.add($scope.selectedMember[i].$id, chat.$id, name, lastText, face, members, chatType, tripId, tripTitle, $rootScope.currentUser.name);
         //$rootScope.currentTrip.travellers.$add({travellerId: $scope.selectedMember[i].$id, displayName: name, profilePic: face});
        }

        Activities.get($rootScope.currentUser.$id, chat.$id).$loaded(function(activities){
          $scope.chat = activities;

          // close
          $ionicHistory.clearCache();
          $scope.closeModal();
          $state.go("tab.group", { "groupId": $scope.chat.$id });
        });
      });

    }
  }
  
  $scope.friendsList = function() {
      ModalService
      .init('templates/friends-list.html', $scope)
      .then(function(modal) {
        modal.show();
      });
  };
  
  $scope.userSettings = function() {
      ModalService
      .init('templates/user-settings.html', $scope)
      .then(function(modal) {
        modal.show();
      });
  };
  
})

.controller('FriendsCtrl', function($scope, Users, $ionicPopup) {
  // get user list
    
    Users.all.$loaded(function(users){

          $scope.users = users;
          for(var i=0; i<$scope.users.length; i++){
            $scope.users[i].checked = null;
          }
    });
  //$scope.users = 'contacts';
    
  // add contact
  $scope.showPromptAdd = function () {
    $ionicPopup.prompt({
        title: 'Invite to Messenger',
        template: 'Enter someone\'s email to invite them on Messenger',
        inputType: 'email',
        inputPlaceholder: 'Email',
        okText: 'Send',
      }
    )
    .then(function (res) {
        console.log('Your password is', res);
    });
  }
})

.controller('AccountCtrl', function($scope, Auth) {
  $scope.signOut = function (){
    return Auth.$signOut();
  };
});
