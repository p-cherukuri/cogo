# Ionic Installation #

1) Clone/Download ZIP  

2) Install latest version of node.js  

3) Install latest version of Cordova 
```
#!Command Line

npm install -g cordova
```


4) Install latest version of Ionic 
```
#!Command Line

npm install -g ionic
```


5) Install latest version of Bower package manager 
```
#!Command Line

npm install -g bower
```


6) Install latest version of Gulp JavaScript build tool 
```
#!Command Line

npm install -g gulp
```

# Running the project #

1) Start a blank new Ionic project using this command: 
```
#!Command Line

ionic start <app name> blank
```


2) Copy the contents of the "www" directory in cloned/downloaded repo to the "www" directory in new blank project    

3) Navigate to new project's root directory and run project in web browser using this command: 
```
#!Command Line

ionic serve
```


4) Changes to contents of "www" directory will be live reloaded when saved. End with: 
```
#!Command Line

q
```

# Running the mockup #

Go to "cogo_mockup" folder and open "index.html"